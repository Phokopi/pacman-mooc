﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pacman.Core
{
    public class Ghost : Living
    {
        #region Fields

        // frames support

        public enum FramesIndexGhost
        {
            RIGHT_1 = 0,
            RIGHT_2 = 1,
            DOWN_1 = 2,
            DOWN_2 = 3,
            UP_1 = 4,
            UP_2 = 5,
            LEFT_1 = 6,
            LEFT_2 = 7
        }

        private FramesIndexGhost frameIndex;

        // Game1.Update is called every 1/60 second. 1 => 16, 2 => 33, etc ...
        private int msBetweenTwoUpdates = (int)Math.Floor(1 / 60f * 1000);

        // Ghost Type

        public enum GhostType
        {
            red,
            blue,
            orange,
            pink
        }

        private GhostType ghostType;

        #endregion Fields

        #region Constructors

        public Ghost(World world, int tileX, int tileY, GhostType ghostType) : base(8, 14, 14)
        {
            frameIndex = FramesIndexGhost.RIGHT_1;

            sourceRectangle.Width = frameWidth;
            sourceRectangle.Height = frameHeight;
            sourceRectangle.X = (int)frameIndex * frameWidth;
            sourceRectangle.Y = 0;

            // set start tile
            startTileX = tileX;
            startTileY = tileY;
            this.tileX = startTileX;
            this.tileY = startTileY;

            // calculate start position from start tile
            position.X = tileX * world.TileSize - (frameWidth / 2 - world.TileSize / 2);
            position.Y = tileY * world.TileSize - (frameHeight / 2 - world.TileSize / 2);

            // collision rectangle
            collisionRectangle = new Rectangle((int)position.X, (int)position.Y, frameWidth, frameHeight);

            // initial direction
            direction = InitialDirection(ghostType);

            // ghost exclusivity
            this.ghostType = ghostType;
        }

        #endregion Constructors

        #region Public methods

        public void LoadContent(Microsoft.Xna.Framework.Content.ContentManager content)
        {
            texture = content.Load<Texture2D>(@"graphics/ghost_8_" + ghostType);
        }

        public void UpdateFrame(GameTime gameTime)
        {
            // update time
            msOnScreen += gameTime.ElapsedGameTime.Milliseconds;

            if (targetSet)
            {
                if (msOnScreen > frameTime)
                {
                    // reset timer
                    msOnScreen = 0;

                    switch (direction)
                    {
                        case PossibleDirections.UP:
                            if (frameIndex == FramesIndexGhost.UP_1)
                                frameIndex = FramesIndexGhost.UP_2;
                            else
                                frameIndex = FramesIndexGhost.UP_1;
                            break;

                        case PossibleDirections.LEFT:
                            if (frameIndex == FramesIndexGhost.LEFT_1)
                                frameIndex = FramesIndexGhost.LEFT_2;
                            else
                                frameIndex = FramesIndexGhost.LEFT_1;
                            break;

                        case PossibleDirections.DOWN:
                            if (frameIndex == FramesIndexGhost.DOWN_1)
                                frameIndex = FramesIndexGhost.DOWN_2;
                            else
                                frameIndex = FramesIndexGhost.DOWN_1;
                            break;

                        case PossibleDirections.RIGHT:
                            if (frameIndex == FramesIndexGhost.RIGHT_1)
                                frameIndex = FramesIndexGhost.RIGHT_2;
                            else
                                frameIndex = FramesIndexGhost.RIGHT_1;
                            break;
                    }
                }

                // update source rectangle
                sourceRectangle.X = (int)frameIndex * frameWidth;
                //sourceRectangle.Y = 0;
            }
        }

        public void Move(GameTime gameTime, KeyboardState keyboard, World world)
        {
            // if not moving yet
            if (!targetSet)
            {
                direction = PickDirection(world);

                switch (direction)
                {
                    case PossibleDirections.UP:
                        // if ghost will move up
                        if (tileY > 0 &&
                        world.GetTile(tileX, tileY - 1) != World.Tile.WALL)
                        {
                            SetTarget(world, tileX, tileY - 1);
                        }
                        break;

                    case PossibleDirections.LEFT:
                        // if ghost will move left
                        if (tileX > 0 &&
                        world.GetTile(tileX - 1, tileY) != World.Tile.WALL)
                        {
                            SetTarget(world, tileX - 1, tileY);
                        }
                        break;

                    case PossibleDirections.DOWN:
                        // if ghost will move down
                        if (tileY < world.NumberTilesDown - 1 &&
                        world.GetTile(tileX, tileY + 1) != World.Tile.WALL
    )
                        {
                            SetTarget(world, tileX, tileY + 1);
                        }
                        break;

                    case PossibleDirections.RIGHT:
                        // if ghost will move right
                        if (tileX < world.NumberTilesRight - 1 &&
                        world.GetTile(tileX + 1, tileY) != World.Tile.WALL)
                        {
                            SetTarget(world, tileX + 1, tileY);
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// update posion of the ghost by moving him and changing his tile when arrived at destination.
        /// </summary>
        /// <param name="gameTime"></param>
        public void UpdatePosition(GameTime gameTime, World world)
        {
            // update timer
            msSinceLastUpdate += gameTime.ElapsedGameTime.Milliseconds;

            if (msSinceLastUpdate >= msBetweenTwoUpdates)
            {
                // reset timer
                msSinceLastUpdate = 0;

                if (targetSet)
                {
                    // update position
                    position.X += (int)velocity.X;
                    position.Y += (int)velocity.Y;

                    // update collision rectangle
                    UpdateCollisionRectanglePosition();

                    // if we finished to move
                    if (world.GetTileCenterX(tileXTarget, tileYTarget) == getCenterFrameX() &&
                        world.GetTileCenterY(tileXTarget, tileYTarget) == getCenterFrameY())
                    {
                        ClearTarget();

                        // we reached the target
                        tileX = tileXTarget;
                        tileY = tileYTarget;
                    }
                }
            }
        }

        public void Reset(World world)
        {
            // go back to start tile
            tileX = startTileX;
            tileY = startTileY;

            // calculate start position from start tile
            position.X = tileX * world.TileSize - (frameWidth / 2 - world.TileSize / 2);
            position.Y = tileY * world.TileSize - (frameHeight / 2 - world.TileSize / 2);

            // collision rectangle
            UpdateCollisionRectanglePosition();

            // initial direction
            direction = InitialDirection(ghostType);

            // reset target
            ClearTarget();
        }

        #endregion Public methods

        #region Private methods

        /// <summary>
        /// Returns the directions where the ghost can move to
        /// (does not take into account the direction which the ghost is coming from)
        /// </summary>
        /// <param name="world">the current world map</param>
        /// <returns>A list of those reachable tiles</returns>
        private List<PossibleDirections> ReachableNeighbors(World world)
        {
            List<PossibleDirections> list = new List<PossibleDirections>();

            // tile up
            if (tileY > 0 &&
            world.GetTile(tileX, tileY - 1) != World.Tile.WALL &&
            direction != GetOpposite(PossibleDirections.UP))
            {
                list.Add(PossibleDirections.UP);
            }

            // tile left
            if (tileX > 0 &&
            world.GetTile(tileX - 1, tileY) != World.Tile.WALL &&
            direction != GetOpposite(PossibleDirections.LEFT))
            {
                list.Add(PossibleDirections.LEFT);
            }
            // tile down
            if (tileY < world.NumberTilesDown - 1 &&
            world.GetTile(tileX, tileY + 1) != World.Tile.WALL &&
            direction != GetOpposite(PossibleDirections.DOWN))
            {
                list.Add(PossibleDirections.DOWN);
            }
            // tile right
            if (tileX < world.NumberTilesRight - 1 &&
            world.GetTile(tileX + 1, tileY) != World.Tile.WALL &&
            direction != GetOpposite(PossibleDirections.RIGHT))
            {
                list.Add(PossibleDirections.RIGHT);
            }

            return list;
        }

        private PossibleDirections GetOpposite(PossibleDirections direction)
        {
            switch (direction)
            {
                case PossibleDirections.DOWN:
                    return PossibleDirections.UP;

                case PossibleDirections.UP:
                    return PossibleDirections.DOWN;

                case PossibleDirections.LEFT:
                    return PossibleDirections.RIGHT;

                case PossibleDirections.RIGHT:
                    return PossibleDirections.LEFT;

                // so that no warning and all possible paths have a return value
                default:
                    return this.direction;
            }
        }

        private bool IsAtJunction(List<PossibleDirections> reachableNeighbors)
        {
            // if at least 2 possible way
            if (reachableNeighbors.Count >= 2)
                return true;
            return false;
        }

        private PossibleDirections PickDirection(World world)
        {
            List<PossibleDirections> reachableNeighbors = ReachableNeighbors(world);

            if (IsAtJunction(reachableNeighbors))
            {
                // if several directions possible
                return reachableNeighbors[Game1.random.Next(0, reachableNeighbors.Count)];
            }
            else if (reachableNeighbors.Count == 1)
            {
                // if only one possible direction (same direction as before OR corner)
                return reachableNeighbors[0];
            }
            else
            {
                // here, we're stuck and need to do a U-turn
                return GetOpposite(direction);
            }
        }

        private PossibleDirections InitialDirection(GhostType ghostType)
        {
            switch (ghostType)
            {
                case GhostType.red: return PossibleDirections.DOWN;
                case GhostType.blue: return PossibleDirections.DOWN;
                case GhostType.orange: return PossibleDirections.UP;
                case GhostType.pink: return PossibleDirections.UP;
                default: return PossibleDirections.DOWN; // so that no warning...
            }
        }

        #endregion Private methods
    }
}