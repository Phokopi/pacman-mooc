﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pacman.Core
{
    public class World
    {
        #region Fields

        private Vector2 position;
        private Texture2D texture;
        private Texture2D pointTexture;

        // grid definition
        public enum Tile
        {
            EMPTY = 0,
            WALL = 1,
            POINT = 2,
            CANDY = 3
        }

        private int worldWidth;
        private int worldHeight;
        private int tileSize;
        private int numberTilesRight;
        private int numberTilesDown;

        // matrix is a matrix of the grid. Beware, first element is Y (top to bottom), 2nd is X (left to right)
        private Tile[,] matrix;

        // number of points left in the game
        private int numberPointsLeft;

        #endregion Fields

        #region Constructors

        public World(int worldNumber)
        {
            worldWidth = 224;
            worldHeight = 248;
            tileSize = 8;
            numberTilesRight = worldWidth / tileSize;
            numberTilesDown = worldHeight / tileSize;

            if (worldNumber == 1)
            {
                matrix = MapsConstant.matrix_world1;
            }
            // else world number is 2
            else
            {
                matrix = MapsConstant.matrix_world2;
            }

            numberPointsLeft = 0;
            for (int y = 0; y < numberTilesDown; y++)
            {
                for (int x = 0; x < numberTilesRight; x++)
                {
                    if (GetTile(x, y) == Tile.POINT)
                    {
                        numberPointsLeft += 1;
                    }
                }
            }
        }

        #endregion Constructors

        #region Properties

        public Texture2D Texture
        {
            set { texture = value; }
        }

        public Texture2D PointTexture
        {
            set { pointTexture = value; }
        }

        public int NumberPointsLeft
        {
            get { return numberPointsLeft; }
            set { numberPointsLeft = value; }
        }

        public int TileSize
        {
            get { return tileSize; }
        }

        public int NumberTilesRight
        {
            get { return numberTilesRight; }
        }

        public int NumberTilesDown
        {
            get { return numberTilesDown; }
        }

        #endregion Properties

        #region Public methods

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
            DrawPoints(spriteBatch);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="x">the x-th tile from left to right</param>
        /// <param name="y">the y-th tile from top to bottom</param>
        /// <param name="type"></param>
        public void SetTile(int x, int y, Tile type)
        {
            matrix[y, x] = type;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="x">the x-th tile from left to right</param>
        /// <param name="y">the y-th tile from top to bottom</param>
        /// <returns></returns>
        public Tile GetTile(int x, int y)
        {
            return matrix[y, x];
        }

        public int GetTileCenterX(int x, int y)
        {
            return tileSize * x + tileSize / 2;
        }

        public int GetTileCenterY(int x, int y)
        {
            return tileSize * y + tileSize / 2;
        }

        #endregion Public methods

        #region Private methods

        private void DrawPoints(SpriteBatch spriteBatch)
        {
            for (int y = 0; y < numberTilesDown; y++)
            {
                for (int x = 0; x < numberTilesRight; x++)
                {
                    if (matrix[y, x] == Tile.POINT)
                    {
                        spriteBatch.Draw(pointTexture, new Vector2(GetTileCenterX(x, y) - 1, GetTileCenterY(x, y)), Color.White);
                    }
                }
            }
        }

        #endregion Private methods
    }
}