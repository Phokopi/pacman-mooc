﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace Pacman.Core
{
    public class Player : Living
    {
        #region Fields

        // frames support

        public enum FramesIndexPlayer
        {
            RIGHT_1 = 0,
            RIGHT_2 = 1,
            RIGHT_3 = 2,
            DOWN_1 = 3,
            DOWN_2 = 4,
            DOWN_3 = 5,
            LEFT_1 = 6,
            LEFT_2 = 7,
            LEFT_3 = 8,
            UP_1 = 9,
            UP_2 = 10,
            UP_3 = 11
        }

        private FramesIndexPlayer frameIndex;

        // Game1.Update is called every 1/60 second. 1 => 16, 2 => 33, etc ...
        private int msBetweenTwoUpdates = (int)Math.Floor(1 / 60f * 1000);

        // score
        private int score;

        // lives
        private int numberLivesInReserve;

        // audio support
        private int nbFramesSinceLastAudio;

        #endregion Fields

        #region Constructors

        public Player(World world, int numberLivesInReserve) : base(12, 14, 14)
        {
            frameIndex = FramesIndexPlayer.RIGHT_1;

            sourceRectangle.Width = frameWidth;
            sourceRectangle.Height = frameHeight;
            sourceRectangle.X = (int)frameIndex * frameWidth;
            sourceRectangle.Y = 0;

            // set start tile
            startTileX = 14;
            startTileY = 23;
            tileX = startTileX;
            tileY = startTileY;

            // calculate start position from start tile
            position.X = tileX * world.TileSize - (frameWidth / 2 - world.TileSize / 2);
            position.Y = tileY * world.TileSize - (frameHeight / 2 - world.TileSize / 2);

            // collision rectangle
            collisionRectangle = new Rectangle((int)position.X, (int)position.Y, frameWidth, frameHeight);

            // initial direction
            direction = PossibleDirections.RIGHT;

            // score and lives
            score = 0;
            this.numberLivesInReserve = numberLivesInReserve;

            // audio support
            nbFramesSinceLastAudio = 0;
        }

        #endregion Constructors

        #region Properties

        public int Score
        {
            get { return score; }
        }

        public int NumberLivesInReserve
        {
            get { return numberLivesInReserve; }
        }

        #endregion Properties

        #region Public methods

        public void LoadContent(Microsoft.Xna.Framework.Content.ContentManager content)
        {
            texture = content.Load<Texture2D>(@"graphics/pacman_12");
        }

        public void UpdateFrame(GameTime gameTime, KeyboardState keyboard)
        {
            // update time
            msOnScreen += gameTime.ElapsedGameTime.Milliseconds;

            // update only if the player wants to move
            bool wantsToMove = keyboard.IsKeyDown(Keys.Z) ||
                keyboard.IsKeyDown(Keys.Q) ||
                keyboard.IsKeyDown(Keys.S) ||
                keyboard.IsKeyDown(Keys.D);

            if (targetSet || wantsToMove)
            {
                if (msOnScreen > frameTime)
                {
                    // reset timer
                    msOnScreen = 0;

                    // 1 frame will be played
                    nbFramesSinceLastAudio += 1;
                    if (nbFramesSinceLastAudio >= 3)
                    {
                        // play sound and reset counter
                        Game1.chomp.Play();
                        nbFramesSinceLastAudio = 0;
                    }
                    switch (direction)
                    {
                        case PossibleDirections.UP:
                            if (frameIndex == FramesIndexPlayer.UP_1)
                            {
                                frameIndex = FramesIndexPlayer.UP_2;
                            }
                            else if (frameIndex == FramesIndexPlayer.UP_2)
                                frameIndex = FramesIndexPlayer.UP_3;
                            else
                                frameIndex = FramesIndexPlayer.UP_1;
                            break;

                        case PossibleDirections.LEFT:
                            if (frameIndex == FramesIndexPlayer.LEFT_1)
                            {
                                frameIndex = FramesIndexPlayer.LEFT_2;
                            }
                            else if (frameIndex == FramesIndexPlayer.LEFT_2)
                                frameIndex = FramesIndexPlayer.LEFT_3;
                            else
                                frameIndex = FramesIndexPlayer.LEFT_1;
                            break;

                        case PossibleDirections.DOWN:
                            if (frameIndex == FramesIndexPlayer.DOWN_1)
                            {
                                frameIndex = FramesIndexPlayer.DOWN_2;
                            }
                            else if (frameIndex == FramesIndexPlayer.DOWN_2)
                                frameIndex = FramesIndexPlayer.DOWN_3;
                            else
                                frameIndex = FramesIndexPlayer.DOWN_1;
                            break;

                        case PossibleDirections.RIGHT:
                            if (frameIndex == FramesIndexPlayer.RIGHT_1)
                            {
                                frameIndex = FramesIndexPlayer.RIGHT_2;
                            }
                            else if (frameIndex == FramesIndexPlayer.RIGHT_2)
                                frameIndex = FramesIndexPlayer.RIGHT_3;
                            else
                                frameIndex = FramesIndexPlayer.RIGHT_1;
                            break;
                    }
                }

                // update source rectangle
                sourceRectangle.X = (int)frameIndex * frameWidth;
                //sourceRectangle.Y = 0;
            }
        }

        public void Move(GameTime gameTime, KeyboardState keyboard, World world)
        {
            // if not moving yet
            if (!targetSet)
            {
                if (keyboard.IsKeyDown(Keys.Z))
                {
                    // we want to move up
                    direction = PossibleDirections.UP;

                    // if we can move up
                    if (tileY > 0 &&
                    world.GetTile(tileX, tileY - 1) != World.Tile.WALL)
                    {
                        SetTarget(world, tileX, tileY - 1);
                    }
                }

                else if (keyboard.IsKeyDown(Keys.Q))
                {
                    // we want to move left
                    direction = PossibleDirections.LEFT;

                    // if we can move left
                    if (tileX > 0 &&
                    world.GetTile(tileX - 1, tileY) != World.Tile.WALL)
                    {
                        SetTarget(world, tileX - 1, tileY);
                    }
                }

                else if (keyboard.IsKeyDown(Keys.S))
                {
                    // we want to move down
                    direction = PossibleDirections.DOWN;

                    // if we can move down
                    if (tileY < world.NumberTilesDown - 1 &&
                    world.GetTile(tileX, tileY + 1) != World.Tile.WALL
)
                    {
                        SetTarget(world, tileX, tileY + 1);
                    }
                }

                else if (keyboard.IsKeyDown(Keys.D))
                {
                    // we want to move right
                    direction = PossibleDirections.RIGHT;

                    // if we can move right
                    if (tileX < world.NumberTilesRight - 1 &&
                    world.GetTile(tileX + 1, tileY) != World.Tile.WALL)
                    {
                        SetTarget(world, tileX + 1, tileY);
                    }
                }
            }
        }

        /// <summary>
        /// update posion of the player by moving him and changing his tile when arrived at destination.
        /// </summary>
        /// <param name="gameTime"></param>
        public void UpdatePosition(GameTime gameTime, World world)
        {
            // update timer
            msSinceLastUpdate += gameTime.ElapsedGameTime.Milliseconds;

            if (msSinceLastUpdate >= msBetweenTwoUpdates)
            {
                // reset timer
                msSinceLastUpdate = 0;

                if (targetSet)
                {
                    // update position
                    position.X += (int)velocity.X;
                    position.Y += (int)velocity.Y;

                    // update collision rectangle
                    UpdateCollisionRectanglePosition();

                    // if we finished to move
                    if (world.GetTileCenterX(tileXTarget, tileYTarget) == getCenterFrameX() &&
                        world.GetTileCenterY(tileXTarget, tileYTarget) == getCenterFrameY())
                    {
                        ClearTarget();

                        // we reached the target
                        tileX = tileXTarget;
                        tileY = tileYTarget;

                        // check if it was a point case
                        if (world.GetTile(tileX, tileY) == World.Tile.POINT)
                        {
                            score += 1;
                            world.SetTile(tileX, tileY, World.Tile.EMPTY);
                            world.NumberPointsLeft -= 1;
                        }
                    }
                }
            }
        }

        public void Reset(World world)
        {
            numberLivesInReserve -= 1;

            // go back to start tile
            tileX = startTileX;
            tileY = startTileY;

            // calculate start position from start tile
            position.X = tileX * world.TileSize - (frameWidth / 2 - world.TileSize / 2);
            position.Y = tileY * world.TileSize - (frameHeight / 2 - world.TileSize / 2);

            // collision rectangle
            UpdateCollisionRectanglePosition();

            // initial direction
            direction = Living.PossibleDirections.RIGHT;

            // reset target
            ClearTarget();
        }

        #endregion Public methods
    }
}