﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pacman.Core
{
    public class Living
    {
        #region Fields

        // position of the living in the matrix/grid

        protected int tileX;
        protected int tileY;
        protected int startTileX;
        protected int startTileY;

        // movement support

        protected Vector2 velocity;
        protected bool targetSet;
        protected int tileXTarget;
        protected int tileYTarget;
        protected float baseSpeed;

        // draw support

        // top left corner of the living
        protected Vector2 position;

        protected Texture2D texture;

        // Rectangle where the frame is in our base image for the living entity
        protected Rectangle sourceRectangle;

        // time
        protected int msOnScreen;

        // frames support

        protected int frameTime;

        protected int totalAnimationFrames;
        protected int frameWidth;
        protected int frameHeight;
        protected int msSinceLastUpdate;

        // direction support
        public enum PossibleDirections
        {
            RIGHT,
            DOWN,
            LEFT,
            UP
        }

        protected PossibleDirections direction;

        // collision support

        protected Rectangle collisionRectangle;

        #endregion Fields

        #region Constructors

        public Living(int totalAnimationFrames, int frameWidth, int frameHeight)
        {
            this.totalAnimationFrames = totalAnimationFrames;
            this.frameWidth = frameWidth;
            this.frameHeight = frameHeight;

            velocity = Vector2.Zero;
            targetSet = false;

            frameTime = 100;
            baseSpeed = 1; // should be an int, since we cast the position into an int
            msSinceLastUpdate = 0;
        }

        #endregion Constructors

        #region Properties

        public Rectangle CollisionRectangle
        {
            get { return collisionRectangle; }
        }

        #endregion Properties

        #region Public methods

        public void DrawAnimation(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, sourceRectangle, Color.White);
        }

        #endregion Public methods

        #region Protected methods

        protected void SetTarget(World world, int tileXTarget, int tileYTarget)
        {
            targetSet = true;

            this.tileXTarget = tileXTarget;
            this.tileYTarget = tileYTarget;

            // update velocity vector
            velocity.X = world.GetTileCenterX(tileXTarget, tileYTarget) - getCenterFrameX();
            velocity.Y = world.GetTileCenterY(tileXTarget, tileYTarget) - getCenterFrameY();

            //// normalise it
            velocity.Normalize();

            // multiply by baseSpeed
            velocity.X *= baseSpeed;
            velocity.Y *= baseSpeed;
        }

        protected int getCenterFrameX()
        {
            return (int)position.X + frameWidth / 2;
        }

        protected int getCenterFrameY()
        {
            return (int)position.Y + frameHeight / 2;
        }

        protected void UpdateCollisionRectanglePosition()
        {// position.X, (int)position.Y, frameWidth, frameHeight);
            collisionRectangle.X = (int)position.X;
            collisionRectangle.Y = (int)position.Y;
        }

        /// <summary>
        /// Clears the target for the living (it no longer has a target)
        /// </summary>
        protected void ClearTarget()
        {
            targetSet = false;
        }

        #endregion Protected methods
    }
}