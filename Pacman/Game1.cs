﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Pacman.Core;
using System;
using System.Collections.Generic;

namespace Pacman
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        // random for all our files, hence the static
        public static Random random = new Random();

        // window

        public const int MARGIN_BOTTOM = 30;
        public const int WINDOW_WIDTH = 224;
        public const int WINDOW_HEIGHT = 248 + MARGIN_BOTTOM;

        // world support

        private World world;
        private const int WORLD_NUMBER = 1; // 1 or 2, your choice

        // player support
        private Player player;

        // text display support

        private SpriteFont font;
        private Vector2 scoreTextLocation = new Vector2(9, 255);

        // lives support

        private Texture2D lifeSprite;
        private Vector2 lifeTextLocation = new Vector2(120, 255);

        // ghosts
        private List<Ghost> ghosts = new List<Ghost>();

        // timer for begining the game

        private const int msCountDownStart = 3000;
        private int msCountDownStartEllapsed = 0;

        // audio

        private static SoundEffect beginning;
        public static SoundEffect chomp;
        private static SoundEffect death;
        private static SoundEffect background;
        private static SoundEffectInstance backgroundInstance;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // window width and height
            graphics.PreferredBackBufferWidth = WINDOW_WIDTH;
            graphics.PreferredBackBufferHeight = WINDOW_HEIGHT;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            // world initializing
            world = new World(WORLD_NUMBER);

            // player initializing
            player = new Player(world, 2);

            // ghosts
            ghosts.Add(new Ghost(world, 1, 1, Ghost.GhostType.red));
            ghosts.Add(new Ghost(world, world.NumberTilesRight - 2, 1, Ghost.GhostType.blue));
            ghosts.Add(new Ghost(world, 1, world.NumberTilesDown - 2, Ghost.GhostType.orange));
            ghosts.Add(new Ghost(world, world.NumberTilesRight - 2, world.NumberTilesDown - 2, Ghost.GhostType.pink));

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            // world map
            world.Texture = Content.Load<Texture2D>(@"graphics/world" + WORLD_NUMBER);
            world.PointTexture = Content.Load<Texture2D>(@"graphics/point_yellow");

            // player pacman
            player.LoadContent(Content);

            // load sprite font
            font = Content.Load<SpriteFont>(@"fonts\score");

            // lives
            lifeSprite = Content.Load<Texture2D>(@"graphics/life");

            // ghosts
            foreach (Ghost ghost in ghosts)
            {
                ghost.LoadContent(Content);
            }

            // load audio content
            //beginning = Content.Load<SoundEffect>(@"audio\beginning");
            chomp = Content.Load<SoundEffect>(@"audio\chomp");
            death = Content.Load<SoundEffect>(@"audio\death");
            background = Content.Load<SoundEffect>(@"audio\background");

            // play background music
            backgroundInstance = background.CreateInstance();
            backgroundInstance.IsLooped = true;
            backgroundInstance.Volume = 0.50f;

            // play beginning audio
            //beginning.Play();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            KeyboardState keyboard = Keyboard.GetState();

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || keyboard.IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            // update timer begining game
            msCountDownStartEllapsed += gameTime.ElapsedGameTime.Milliseconds;

            // if cooldown to start the game is over
            if (msCountDownStartEllapsed >= msCountDownStart)
            {
                // if game still running
                if (player.NumberLivesInReserve >= 0 &&
                    world.NumberPointsLeft >= 1)
                {
                    // play background music
                    backgroundInstance.Play();

                    // move player
                    player.Move(gameTime, keyboard, world);
                    player.UpdateFrame(gameTime, keyboard);
                    player.UpdatePosition(gameTime, world);

                    // move ghosts
                    foreach (Ghost ghost in ghosts)
                    {
                        ghost.Move(gameTime, keyboard, world);
                        ghost.UpdateFrame(gameTime);
                        ghost.UpdatePosition(gameTime, world);
                    }

                    // check for collisions
                    bool hasCollided = false;
                    foreach (Ghost ghost in ghosts)
                    {
                        if (player.CollisionRectangle.Intersects(ghost.CollisionRectangle))
                        {
                            hasCollided = true;
                        }
                    }
                    if (hasCollided)
                    {
                        // reset timer
                        msCountDownStartEllapsed = 0;

                        // reset player
                        player.Reset(world);

                        // reset ghosts
                        foreach (Ghost ghost in ghosts)
                        {
                            ghost.Reset(world);
                        }

                        // play death music
                        death.Play();
                    }
                }
                // end of game
                else
                {
                    backgroundInstance.Stop();
                }
            }
            else
            {
                backgroundInstance.Stop();
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here

            spriteBatch.Begin();

            if (player.NumberLivesInReserve >= 0 && world.NumberPointsLeft >= 1)
            {
                // world map
                world.Draw(spriteBatch);

                // player animations
                player.DrawAnimation(spriteBatch);

                // score at the bottom
                spriteBatch.DrawString(font, "Score : " + player.Score, scoreTextLocation, Color.White);

                // player lives in reserve
                spriteBatch.DrawString(font, "Lives : ", lifeTextLocation, Color.White);
                for (int i = 0; i < player.NumberLivesInReserve; i++)
                    spriteBatch.Draw(lifeSprite, new Rectangle((int)lifeTextLocation.X + 52 + (i * 14), 261, lifeSprite.Width, lifeSprite.Height), Color.White);

                // ghosts animations
                foreach (Ghost ghost in ghosts)
                {
                    ghost.DrawAnimation(spriteBatch);
                }
            }
            else if (world.NumberPointsLeft == 0)
            {
                world.Draw(spriteBatch); // world
                player.DrawAnimation(spriteBatch); // immobile player
                spriteBatch.DrawString(font, "You won ! score : " + player.Score + ", lives : " + player.NumberLivesInReserve, scoreTextLocation, Color.White);
            }
            else if (player.NumberLivesInReserve == -1)
            {
                world.Draw(spriteBatch); // world
                spriteBatch.DrawString(font, "Game over ! score : " + player.Score, scoreTextLocation, Color.White);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}