\select@language {english}
\contentsline {section}{\numberline {1}MOOC presentation}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Course introduction}{1}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Teacher}{1}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Learning objectives}{1}{subsubsection.1.1.2}
\contentsline {subsubsection}{\numberline {1.1.3}Course structure}{2}{subsubsection.1.1.3}
\contentsline {paragraph}{Course project\\}{2}{section*.4}
\contentsline {subsection}{\numberline {1.2}MOOC validation}{3}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}Monitoring}{3}{subsubsection.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.2}Final grade}{3}{subsubsection.1.2.2}
\contentsline {section}{\numberline {2}Modules}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Module 1 : Introduction}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Module 2 : First C\# Program}{5}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Executing our code}{5}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Comments}{5}{subsubsection.2.2.2}
\contentsline {paragraph}{Documentation comments\\}{5}{section*.7}
\contentsline {paragraph}{Line comments\\}{5}{section*.9}
\contentsline {subsubsection}{\numberline {2.2.3}Coding standards}{6}{subsubsection.2.2.3}
\contentsline {subsection}{\numberline {2.3}Module 3 : Data Types, Variables, and Constants}{7}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Module 4 : Classes and Objects}{7}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Module 5 : XNA Basics}{8}{subsection.2.5}
\contentsline {subsubsection}{\numberline {2.5.1}General ideas for game development}{8}{subsubsection.2.5.1}
\contentsline {subsubsection}{\numberline {2.5.2}XNA for game development}{9}{subsubsection.2.5.2}
\contentsline {subsubsection}{\numberline {2.5.3}Simple drawing example}{10}{subsubsection.2.5.3}
\contentsline {subsection}{\numberline {2.6}Module 6 : Strings}{11}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Module 7 : Selection}{12}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}Module 8 : XNA Mice and Controllers}{13}{subsection.2.8}
\contentsline {subsubsection}{\numberline {2.8.1}Mouse location processing}{13}{subsubsection.2.8.1}
\contentsline {subsubsection}{\numberline {2.8.2}Mouse button processing}{14}{subsubsection.2.8.2}
\contentsline {paragraph}{The \texttt {Random} class\\}{14}{section*.23}
\contentsline {paragraph}{Checking if a button is pressed\\}{14}{section*.24}
\contentsline {paragraph}{Processing a click\\}{14}{section*.25}
\contentsline {subsubsection}{\numberline {2.8.3}Controller thumbstick and button processing}{15}{subsubsection.2.8.3}
\contentsline {subsection}{\numberline {2.9}Module 9 : Arrays and Collection Classes}{16}{subsection.2.9}
\contentsline {subsubsection}{\numberline {2.9.1}Arrays}{16}{subsubsection.2.9.1}
\contentsline {subsubsection}{\numberline {2.9.2}Collection classes}{17}{subsubsection.2.9.2}
\contentsline {subsection}{\numberline {2.10}Module 10 : Iteration}{18}{subsection.2.10}
\contentsline {subsubsection}{\numberline {2.10.1}For Loops}{18}{subsubsection.2.10.1}
\contentsline {paragraph}{Example using a for loop\\}{18}{section*.32}
\contentsline {paragraph}{For loop and \texttt {Remove} method\\}{19}{section*.36}
\contentsline {subsubsection}{\numberline {2.10.2}Foreach loops}{20}{subsubsection.2.10.2}
\contentsline {subsubsection}{\numberline {2.10.3}While loops}{20}{subsubsection.2.10.3}
\contentsline {subsection}{\numberline {2.11}Module 11 : Class Design and Implementation}{21}{subsection.2.11}
\contentsline {subsection}{\numberline {2.12}Module 12 : XNA Audio}{22}{subsection.2.12}
\contentsline {paragraph}{Playing a sound effect\\}{22}{section*.41}
\contentsline {paragraph}{Playing a background music\\}{22}{section*.45}
\contentsline {subsection}{\numberline {2.13}Module 13 : XNA Text IO}{23}{subsection.2.13}
\contentsline {subsubsection}{\numberline {2.13.1}XNA keyboard input}{23}{subsubsection.2.13.1}
\contentsline {subsubsection}{\numberline {2.13.2}XNA text output}{25}{subsubsection.2.13.2}
\contentsline {section}{\numberline {3}MOOC project}{26}{section.3}
\contentsline {paragraph}{Project increment 1\\}{26}{section*.52}
\contentsline {paragraph}{Project increment 2\\}{27}{section*.54}
\contentsline {paragraph}{Project increment 3\\}{28}{section*.56}
\contentsline {paragraph}{Project increment 4\\}{30}{section*.59}
\contentsline {paragraph}{Project increment 5\\}{31}{section*.61}
\contentsline {section}{\numberline {4}Personal project}{32}{section.4}
\contentsline {subsection}{\numberline {4.1}Introduction}{32}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Development}{33}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}World map}{33}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Player : Pacman}{36}{subsubsection.4.2.2}
\contentsline {paragraph}{Move Pacman\\}{36}{section*.70}
\contentsline {paragraph}{Sprite animations\\}{38}{section*.73}
\contentsline {paragraph}{Player's score and lives\\}{39}{section*.77}
\contentsline {subsubsection}{\numberline {4.2.3}Ghosts}{40}{subsubsection.4.2.3}
\contentsline {paragraph}{Move Ghosts\\}{40}{section*.79}
\contentsline {paragraph}{Sprite animations\\}{40}{section*.80}
\contentsline {subsubsection}{\numberline {4.2.4}Collision between Pacman and ghosts}{41}{subsubsection.4.2.4}
\contentsline {subsubsection}{\numberline {4.2.5}End of the game}{41}{subsubsection.4.2.5}
\contentsline {subsubsection}{\numberline {4.2.6}Music}{42}{subsubsection.4.2.6}
\contentsline {section}{\numberline {5}Conclusion}{43}{section.5}
