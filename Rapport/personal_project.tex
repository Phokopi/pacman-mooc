\section{Personal project}

\subsection{Introduction}

We had to do a personal project for the MOOC oral presentation.\\
This section has been added to my report in January 2018, after I finished the game I developed.\\

The game I developed is a simplified Pac-Man-like game.\\
Here's a screenshot of the original complete Pac-Man game :

\begin{figure}[!h]
 \begin{center}
  \includegraphics[width=8cm]{pp/original.png}
 \end{center}
\end{figure}

Here's what I simplified compared to the original game :
\begin{itemize}
    \item Only one game is played. It means that when you win or die, you are stuck and cannot play again, you have to exit and relaunch the game to do so.
    \item There are no candies to eat ghosts, so you just cannot eat ghosts, you will have to avoid them.
    \item The tunnels are not working. The entrance of each tunnel is coded as a wall.
    \item The ghosts do not start in the center rectangle : since there are 4 ghosts, I made them start at each corner of the map.
    \item Ghosts are not intelligent : they don't chase Pacman, but rather choose their path at random.
    \item There is no animation when Pacman dies.
\end{itemize}

In my game, the player's goal is to collect all the dots (I also call them "points") while avoiding the ghosts.\\

\newpage
\subsection{Development}

In this subsection, I will give some details about the steps I followed when developing my game.\\
Of course, I won't comment every line of code I wrote, but only the parts that are interesting.\\

\subsubsection{World map}

The first thing to think about is how will the world map be coded in Monogame.\\

The world map has its own class in my code.\\

I decided that I would use this sprite from \href{https://www.spriters-resource.com/game_boy_advance/namcomuseum/sheet/22732/}{The Spriters Resource} :

\begin{figure}[!h]
 \begin{center}
  \includegraphics[width=10cm]{pp/source.png}
 \end{center}
\end{figure}

There are four world maps that I can use, but I only tried to implement two of them, because the steps to add one more map would exactly be the same as I did with the first two.\\

About the \textbf{dots} : when Pacman eats a dot, it has to disappear, so I just cannot use the map "as is" !\\
I had to remove the dots from the image, and then draw them myself within the code. We'll get back to that later on.\\

How to actually \textbf{code the world map} ? This Pacman game is totally compatible with the use of a grid : Pacman can move up, down, left and right in a limited amount of space.\\

I divided the world map in a grid in which each tile (square) is 8 pixels large. For example, for the first world map :

\begin{figure}[!h]
 \begin{center}
  \includegraphics{pp/world1grid.png}
 \end{center}
\end{figure}

Then, I wrote a script in Node.js that would convert my world map with a grid to a matrix in C\#. I used Node.js over C\# because it is only a quick script that would help me code the world map, not the actual game, and I already used Node modules to analyze images, so I knew how to do it.\\
You can find this script in the annex.\\

This script will check in every tile if there is a dot, checking the color at the appropriate pixel. Else, if at least one corner of the tile is the same color as walls, then it's a wall. Otherwise, the tile is empty.\\
This works almost perfectly : the wall from where the ghosts initially spawn are detected as "empty" by this script, so we have to be careful about these two tiles :

\begin{figure}[!h]
 \begin{center}
  \includegraphics[width=2cm]{pp/wallnotdetected.png}
 \end{center}
\end{figure}

Each tile of the grid can either be empty, be a wall, or contain a dot :

\begin{figure}[!h]
 \begin{center}
  \includegraphics[height=3cm]{pp/enum.png}
 \end{center}
\end{figure}

When there is a candy in a tile, I decided to consider and code it as a point.\\

\newpage

I had a bit of trouble accessing correctly my tiles in my code.\\
Indeed, a tile can be represented by its (x, y) coordinates :

\begin{figure}[!h]
 \begin{center}
  \includegraphics{pp/xytile.png}
 \end{center}
\end{figure}

The fact is I coded my grid as a matrix : I access the tile at (x, y) with : \texttt{matrix[y, x]}.\\
That's a tricky point, so to not be confused I'm using this method I wrote whenever accessing a tile (I wrote a similar method when setting a Tile) :

\begin{figure}[!h]
 \begin{center}
  \includegraphics{pp/accesstile.png}
 \end{center}
\end{figure}

\newpage
Finally, to draw the world map, we first draw the "empty image" of the map without the dots, that is to say an image like this one :

\begin{figure}[!h]
 \begin{center}
  \includegraphics{pp/world1.png}
 \end{center}
\end{figure}

And then we check for every tile if it is a point, to draw it accordingly.\\


\subsubsection{Player : Pacman}
To begin with, Pacman and the ghosts will share lots of properties and methods. I chose to use inheritance of a \texttt{Living} class, even if we did not learn this principle during the course. I'm using it with very basic notions, so nothing hard here since I already learned inheritance in Java and C++ at ENSIIE.\\

\paragraph{Move Pacman\\}

My first goal was to be able to move Pacman anywhere in the screen where there are no walls, and of course clamped inside the game window.\\

Since my game is based on a grid, I chose to put Pacman in a tile at the beginning of the game, and then he would only be able to go to the 4 nearby tiles (up, down, left, right).\\

At first, whenever the player pressed a key to move to another tile, I just changed the Pacman's location, but of course it was very abrupt and not enjoyable.\\

To make the movement smoother, I decided that when a key to move is pressed, I'd rather set the tile we want to move to as a target for Pacman, and then Pacman will move smoothly toward that target.\\
Of course, the player is unable to change Pacman's direction until he has reached its target.\\
This way, it's way more smooth. We are also able to change in the code Pacman's speed by deciding how often he will update its position, thanks to a timer.\\

I implemented this inside two methods in the \texttt{Pacman} class, \texttt{Move} and \texttt{UpdatePosition}

\begin{figure}[!h]
 \begin{center}
  \includegraphics[height=9cm]{pp/move.png}
 \end{center}
\end{figure}

\begin{figure}[!h]
 \begin{center}
  \includegraphics[width=13cm]{pp/updateposition.png}
 \end{center}
\end{figure}

As you can see in the \texttt{UpdatePosition} method above, I also check if the tile we arrived in contains a point/dot : if so, we increment the player's score by 1, and we set the tile to "empty".\\


\paragraph{Sprite animations\\}

Now that Pacman can move all around the screen, I had to make him change sprites.\\

In the sprite I downloaded, there are 3 images for Pacman, and 3 images for Ms. Pacman. I chose to use the images of Ms. Pacman.\\

Basically, when moving in the same direction, Pacman will alternate between 3 images. To that extent, I created a \texttt{png} file with 12 square of length 14x14 pixels with Pacman's stances, and then we just have to pick the appropriate square/stance in this file when we want to :

\begin{figure}[!h]
 \begin{center}
  \includegraphics{pp/12.png}
 \end{center}
\end{figure}

We change the animation image only when the player wants to move (i.e. is pressing a key) or when Pacman is moving (having a target set).\\

Also, we won't change Pacman's image every frame of the game, it would be too fast. I use a timer to control the time we change the frame, and I put it by default to 100 ms.\\

\begin{figure}[!h]
 \begin{center}
  \includegraphics[height=12cm]{pp/updateframe.png}
 \end{center}
\end{figure}


\newpage

Now, when we want to update Pacman's position and image in the \texttt{Game1} class, we just call them in this order :

\begin{figure}[!h]
 \begin{center}
  \includegraphics[width=7cm]{pp/updatepacman.png}
 \end{center}
\end{figure}


\paragraph{Player's score and lives\\}

Pacman's score is just an int. The player starts with a score of 0 and earn one point each time they eat a dot.\\

Likewise, pacman's lives are just an int, and represent the "lives in reserve". It means that if I give you 3 chances to beat the game, you will have 2 lives in reserve.\\
When you have died once, you'll have 1 live in reserve, and when you have died twice you have 0 live in reserve, but you are still alive. When you have no more lives in reserve and you die, it's game over !\\

We display those 2 data at the bottom of the screen, so our window's height is now equal to the height of our world map image plus some pixels to display data.\\

We draw those data in the \texttt{Draw} method in \texttt{Game1} :

\begin{figure}[!h]
 \begin{center}
  \includegraphics{pp/scorelives.png}
 \end{center}
\end{figure}

\newpage
\subsubsection{Ghosts}

In my game, ghosts cannot die (cannot be eaten by Pacman), so it is pretty simple to initialize 4 of them once and for all at the beginning of the game.\\

\paragraph{Move Ghosts\\}

Ghosts movement mechanics is almost exactly the same as Pacman's.\\
The only difference is that they won't move based on the user's input, but they will make they own way through the world map.\\

In the original Pac-Man game, they have different behavior like turning around or chasing Pacman, but I didn't do an IA this type in my game.\\

So, whenever ghosts arrive on a tile, they will choose a destination at random among those possible, barring the tile from which they are coming from.\\
The only exception to this is when a ghost is blocked in a dead end and needs to do a U-turn.\\

To do so, we create a list of the possible new directions the ghost can go to.\\
If there are at least 2 directions (junction), we pick one at random. Else if there is only one direction we choose that one (corner or straight line). Else there is no new direction, so that's when we need to do a U-turn.\\

\paragraph{Sprite animations\\}

We only have 2 different images provided by the source sprite for our ghosts, unlike Pacman which had 3 images.\\

Except from that, the animation follows the same logic.\\

\begin{figure}[!h]
 \begin{center}
  \includegraphics{pp/ghost_8_blue.png}
 \end{center}
\end{figure}

\begin{figure}[!h]
 \begin{center}
  \includegraphics{pp/ghost_8_orange.png}
 \end{center}
\end{figure}

\begin{figure}[!h]
 \begin{center}
  \includegraphics{pp/ghost_8_pink.png}
 \end{center}
\end{figure}

\begin{figure}[!h]
 \begin{center}
  \includegraphics{pp/ghost_8_red.png}
 \end{center}
\end{figure}


\newpage

\subsubsection{Collision between Pacman and ghosts}

Since we learned collisions using rectangles in the MOOC, I quite naturally decided to use Collision Rectangles for the living entities.\\

The collision rectangle is exactly the same size as the living's sprite size, and is updated whenever the living entity moves.\\

On every update of the main game loop (every 1/60 seconds), we check if Pacman's collision rectangle intersects with (at least) one of the ghost's collision rectangle.\\

When the player hits a ghost, he looses one life and all the living entities (Pacman and the 4 ghosts) get back to their original position.\\


\subsubsection{End of the game}

The game ends whenever the player has used all of their available lives, or when the player succeeded to collect all the dots in the map.\\

In these cases, we draw the final score and an appropriate message ("You won/lost").

\begin{figure}[!h]
 \begin{center}
  \includegraphics[height=6.2cm]{pp/gameover.png}
 \end{center}
\end{figure}

\begin{figure}[!h]
 \begin{center}
  \includegraphics[height=6.2cm]{pp/youwon.png}
 \end{center}
\end{figure}


\newpage

\subsubsection{Music}
I added 3 sounds effect : a sound that will play when Pacman is moving, a sound when it dies, and a background looped music.\\

So that there are not tons of sound whenever Pacman moves by a pixel, I only played the sound when it moves whenever I changed the animation (which is limited with a timer as previously explained).\\

When the game ends, I stop the background music.\\

Moreover, when starting the game, I added 3 seconds where everything is frozen, so that the player has got enough time to put his fingers onto his keyboard. During these 3 seconds, the background music won't be played.\\
Also, these 3 seconds where no music is played and everything is frozen is also here when the player dies (and everybody is teleported back to its original location).\\
