(Tuto : http://franckh.developpez.com/tutoriels/csharp/monogame/part-I/)

Classe world, et classe living. Living représente les objets vivants qui bougent, comme player et enemies qui vont en hériter.

Téléchargement de sprites sur https://www.spriters-resource.com/game_boy_advance/namcomuseum/sheet/22732/
decoupe de l'image source pour avoir une world map. Enlèver les points blancs à la main sur gimp.
Ajout de la taille de la fenetre dans game1.cs = taille de l'image world.png
on build le fichier world avec pipeline et on l'ajoute au contenu dans visual studio.
charge le contenu dans loadcontent
draw le contenu dans draw

Living :

## but : pouvoir bouger pacman dans tous les sens, avec les animations qui changent

image du pacman : on a que 3 images, donc on créer une base avec 12 images (3 par côté) en ayant en tout 12 carrés de taille 14*14 pixels qui se touchent. osef si pas joli pour le ruban de la pacman.
ajout de variables à la classe du player (pour le pacman).
Constructeur de player appelle celui de living avec 12 frames, et taille 14x14 pixels.
méthode move pour changer la position du pacman
méthode updateframe pour changer la frame en cours
bon là il peut bouger dans tous les sens en même temps, et peut sortir de la fenêtre, faudra régler ça ^^

## but : gérer les collisions
pour faire ça on pourrait donner une position à chacun de nos "murs", mais pas très pratique car si on change de terrain, faut remettre de nouveaux murs avec leur nouvelle position ... pas top : on va plutôt checker tout ça avec les couleurs ! ça va donc être une collision au pixel prêt.
ajout de world dans living/player. init dans le constructeur.

On modifie la méthode Move de Player pour empêcher les collisions :
> Décortiquons un peu tout cela ensemble en commençant par la gestion d'une touche (prenons la touche D). On assigne la direction comme avant sinon on ferait du sur-place, ensuite on appelle notre méthode statique Collided. Si elle renvoie false comme le test qu'on fait ici, nous ne sommes pas en collision. Entre en jeu maintenant notre variable collidedDirection ! Voyons à quoi elle va nous servir réellement ! Si par exemple, on se dirige vers la droite, tout droit vers le mur, d'un coup on s'arrête. Normal me diriez-vous. Pour pouvoir changer de direction, il faut détecter si la touche qu'on enfonce est différente par rapport à la direction où on a réalisé notre récente collision. C'est ce que permet cette variable ! Avec un simple test, si on prend le test de la touche Q pour la gauche, on teste si la touche est différente et vu que c'est le cas (on avait une collision par la droite), on change notre variable de collision à sa valeur par défaut NONE puis on décrémente notre position X vu que nous nous dirigeons vers la gauche ! C'est tout simple non ?

on stocke les couleurs des pixels de la map world dans un tableau de taille width * height (pas pratique pour accéder aux indices !).

finalement la map plutôt comme une grille de 28 x 36 tiles. on stock ça dans une matrice, avec à chaque case 0 = rien, 1 = mur, 2 = point, 3 = bonbon.
Quand un point ou un bonbon est mangé par pacman, on passe à une case vide.


on génère la map stockée dans un Tile[,] grâce à un script écrit en javascript. Grâce à une detection par pixels dans chaque case, on stock wall, none, ou point. On verra pour les bonbons plus tard.

En fait on va pas faire de système de collision de pacman avec les murs, trop chiant. On va plutôt faire un quadrillage, et pacman ne peut se déplacer que sur les cases qui ne sont pas des WALLS.
Pour gérer les déplacements, on regarde quelle touche est pressée, et on regarde la case strictement au dessus/dessous/droite/gauche. Si c'est un wall on fait rien, si c'est pas un wall, on déclare que notre destination est celle case (le milieu). Tant que notre position (le milieu de notre pacman) n'est pas égale à notre destination (milieu de la case adjacente), on bloque les déplacements. à chaque frame, on change la position en fonction du temps écoulé et de notre vecteur velocity (différence de la case de destination et de la case de départ). Dès qu'on est arrivé, on change la case actuelle comme étant la case de destination, et on indique qu'on a plus de destination (comme ça on peut à nouveau bouger). wlh

# Gestion des points
on créer un champ point pour le joueur. De suite, on affiche ce score, comme ça quand on ajoutera du code pour augmenter les points on pourra voir si ça marche ou pas. Avec une font, on positionne l'affichage du score en bas (on élargie la fenetre vers le bas) pour pas gêner les calculs qui sont déjà faits avec la taille de la grille (on pourrait rajouter une constante, mais comme il suffit de rajouter vers le bas pour pas s'embêter avec la constante, bah autant faire vers le bas).

**Collecte :** Quand pacman arrive sur une case où y'a un point, on enlève le point, on ajoute +1 au score de pacman.

# Fin du jeu

on ajoute la vie à pacman (2 vie en réserve au début, donc 3 chances en tout de finir le jeu). Fin du jeu quand plus de vie (-1 car 2 puis 1 puis 0 vie en réserve). Aussi, fin du jeu quand tous les points ont été mangés.
On arrête de Update tout dans ces 2 cas, et on affiche dans Draw la map, éventuellement le pacman en cas de victoire, et un message de victoire/défaite en bas de l'écran.

# Fantômes

loadcontent directement dans la classe du fantôme, pour pouvoir load selon le type du fantôme. btw type du fantôme donné dans le constructeur pour modifier la variable "ghostType" de la classe.

Dans Game1 on crée une `List<Ghost>` pour ajouter nos fantômes.
On ajoute les 4 fantômes à la main dans Initialize, puis dans LoadContent on fait un foreach en appelant LoadContent de la classe Ghost.

On fait commencer les fantômes dans les coins, pas besoin de se faire chier à les sortir du milieu comme ça.
on initialise la direction initial de telle sorte qu'on croit qu'ils viennent de l'extérieur. Comme ça, ils croient arriver à une jonction (alors qu'ils sont sur un coin) et choisissent bien une direction au hasard.

Aléatoire : Random random dans Game1 public appelé pour utiliser random.Next() dans tout le jeu et avoir un vrai aléatoire (un seul seed sur tout notre jeu).
A chaque fois qu'il a la possibilité de changer de direction, le fantôme choisit aléatoirement (jonction). Quand il arrive dans un coin, il prend la seule direction possible.

# Collision pacman / fantôme

Ajoute d'un timer en début de partie de 3 secondes, pour que le joueur commence en même temps que les fantômes.
Dès qu'y'a collision, on remet ce timer à 0 pour lui redonner ce gameplay quoi.

On vérifie chaque collision avec les CollisionRectangle, qui sont des rectangles qui commencent sur le point en haut à gauche et ont pour longueur/largeur le width et height du pacman/ghost (comme draw rectangle quoi).
pour ça, on vérifie la collision entre pacman et chacun des ghosts.

on fait pas de déférencement pour la mémoire. Plutôt, on remet nos variables de position, de vie, de direction comme au début de la partie, pour pacman/fantôme. Surtout, ne pas oublier d'enlever le target de pacman/ghost car sinon ça continuera à bouger lol !!


# Music
3 musiques : background, quand pacman avance, quand pacman meurt. Définies en static, car besoin que ce soit static ou alors pas avoir d'instance (mais là on en a bien une à cause du loadcontent) quand on l'appelle pour le déplacement de pacman depuis la classe pacman.
- On joue le background en loop dès qu'on peut bouger. On l'arrête et on la remet après chaque mort (dès qu'on peut rebouger donc).
- quand pacman avance, pour pas avoir trop de sons à chaque tile, on joue le son plutôt toutes les 3 frames (par exemple si le joueur va en ligne droite toute les 1 animation complète). Comme ça on a un truc régulier, et qui dépend pas de la direction où on va, et qui joue même quand on "essaye d'aller dans un mur".
- quand pacman meurt, musique de death.
